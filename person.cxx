#include <string>
#include <map>

//#include <string_view>
//#include "misc.h"
#include <boost/utility/string_ref.hpp>

#include <iostream>

using StringView = boost::string_ref;

struct Person
{
    StringView name() const { return name_; }
    int age() const { return age_; }

    Person(std::string name, int age)
        : name_(std::move(name))
        , age_(age)
    {}

private:
    std::string name_;
    int age_;
};

using PersonMap = std::map<StringView, Person>;

void insert1(PersonMap & dict, std::string name, int age)
{
    Person person(std::move(name), age);
    auto key = person.name();
    dict.emplace(key, std::move(person));
}

void insert2(PersonMap & dict, std::string name, int age)
{
    StringView key = name;
    dict.emplace(std::piecewise_construct, std::forward_as_tuple(key), std::forward_as_tuple(std::move(name), age));
}

/// Test code

std::ostream& operator<<(std::ostream& o, Person const & p) {
    o << p.name() << " (" << p.age() << ")";
    return o;
}
 
int main(int argc, char* argv[])
{
    PersonMap dict;
    insert1(dict, "Name1loooooooooooooooooooooooooooooooong", 23); // OK in my case
    insert1(dict, "Name1", 23); // BAD key

    insert2(dict, "Name2loooooooooooooooooooooooooooooooong", 27); // OK in my case
    insert2(dict, "Name2", 27); // BAD key

    for (auto const & kv : dict) {
        std::cout << kv.first << ": " << kv.second << std::endl;
    }
    return 0;
}
