#include <map>
#include <future>
#include <iostream>
#include <iomanip>
#include <random>

/// fixed version

using Histogram = std::map<int, int>;

void draw(Histogram const & hist)
{
    for (auto const & p : hist)
    {
        std::cout << std::fixed << std::setprecision(1) << std::setw(2)
            << p.first << ' ' << std::string(p.second / 200, '*') << '\n';

    }

}

int main()
{
    std::random_device rdevice;
    std::default_random_engine rengine(rdevice());
    std::normal_distribution<double> dist(0., 1.);
    std::vector<std::future<double>> samples;

#ifndef NDEBUG
    // Issue description
    // Let's look at the example:

    // e1 and e2 are different objects (copy of rengine) with identical independent internal states
    auto e1 = rengine;
    auto e2 = rengine;

    // dist1 and dist2 are different objects (copy of dist) with identical independent internal states
    auto dist1 = dist;
    auto dist2 = dist;

    // dist1 calls operator() of e1 causing advance of dist1 and e1 internal states
    // ... and independently
    // dist2 calls operator() of e2
    // ... as result
    // dist1(e1) and dist2(e2) returns equal values
    std::cout << dist1(e1) << std::endl;
    std::cout << dist2(e2) << std::endl;
#endif

    // pass arguments to 'bind' by reference!
    //
    // according to ccpref:
    // The arguments to bind are copied or moved,
    // and are never passed by reference unless wrapped in std::ref or std::cref.
    auto gen = std::bind(std::ref(dist), std::ref(rengine));

    for (size_t i = 10000; i --> 0; )
        samples.push_back(std::async(std::launch::deferred, gen));

    Histogram hist;
    for (auto & sample : samples)
        ++hist[std::round(sample.get())];
    draw(hist);

}
