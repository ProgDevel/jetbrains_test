#include <string>
#include <map>

//#include <string_view>
//#include "misc.h"
#include <boost/utility/string_ref.hpp>

#include <iostream>

using StringView = boost::string_ref;

struct Person
{
    StringView name() const { return name_; }
    int age() const { return age_; }

#ifndef NDEBUG
    Person(Person &&other) : age_(other.age_) {
        std::cout << "OLD DATA: " << (void *)other.name_.data() << std::endl;
        std::swap(name_, other.name_);
        std::cout << "NEW DATA: " << (void *)name_.data() << std::endl;
    }
#endif

    Person(std::string name, int age)
        : name_(std::move(name))
        , age_(age)
    {}

private:
    std::string name_;
    int age_;
};

//using PersonMap = std::map<StringView, Person>;
// XXX use std::string as map key
using PersonMap = std::map<std::string, Person>;

// XXX pass name by reference
void insert1(PersonMap & dict, const std::string& name, int age)
{
    // XXX copy name as key
    dict.emplace(name, Person(name, age));
}

void insert2(PersonMap & dict, const std::string& name, int age)
{
    dict.emplace(std::piecewise_construct, std::forward_as_tuple(name), std::forward_as_tuple(name, age));
}

/// Test code

std::ostream& operator<<(std::ostream& o, Person const & p) {
    o << p.name() << " (" << p.age() << ")";
    return o;
}
 
int main(int argc, char* argv[])
{
    PersonMap dict;
    insert1(dict, "Name1", 23);
    insert2(dict, "Name2", 27);

    for (auto const & kv : dict) {
        std::cout << kv.first << ": " << kv.second << std::endl;
    }
    return 0;
}
