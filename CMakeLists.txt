cmake_minimum_required(VERSION 2.0)
project(TestProject)

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "RelWithDebInfo" CACHE STRING
        "Choose the type of build, options are: Debug Release
        RelWithDebInfo MinSizeRel."
        FORCE)
endif()

set(Boost_USE_STATIC_LIBS OFF)
set(Boost_USE_MULTITHREADED ON)
set(Boost_USE_STATIC_RUNTIME OFF)
find_package( Boost REQUIRED COMPONENTS )

include_directories(${Boost_INCLUDE_DIR})
message(${Boost_INCLUDE_DIR})

file(GLOB APP_SRCS *.cxx *.cpp)

set(COMMON_CXX_FLAGS "-stdlib=libc++ -std=c++1z -Wall")
set(COMMON_EXE_LINKER_FLAGS "-pthread")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${COMMON_CXX_FLAGS}")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${COMMON_EXE_LINKER_FLAGS}")

foreach(app_src ${APP_SRCS})
    get_filename_component(target ${app_src} NAME_WE)
    add_executable(${target} ${app_src})
    target_link_libraries(${target} LINK_PUBLIC ${Boost_LIBRARIES})
endforeach()
