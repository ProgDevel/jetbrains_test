#pragma once

#include <string>
#include <cstring>
#include <array>
#include <iostream>

namespace misc {

// Custom implementation of string_view
struct string_view
{
    using String = std::string;

    string_view() = default;
    string_view(const string_view& other) = default;

    string_view(const String& str)
        : ptr(str.data()), size(str.length())
    {}

    string_view(const char* str)
        : ptr(str), size(::strlen(str))
    {}

    string_view(const char* str, size_t len)
        : ptr(str), size(len)
    {}

    // Protection for temporary objects
    string_view(String&& str) = delete;

    string_view& operator=(const string_view&) = default;

    bool operator < (const string_view& s2) const {
        int cmpret = ::strncmp(ptr, s2.ptr, std::min(size, s2.size));
        return cmpret < 0 || (cmpret == 0 && size < s2.size);
    }

    const char* begin() const { return ptr; }
    const char* end() const { return ptr + size; }

private:
    const char *ptr = nullptr;
    size_t      size = 0;
};

}

std::ostream& operator<<(std::ostream& o, misc::string_view const & s) {
    std::copy(s.begin(), s.end(), std::ostream_iterator<char>(o, ""));
    return o;
}


