#include <string>

// Oops! My compiler (clang-3.8, std=c++1z) does not support for this
//#include <string_view>
#include <boost/utility/string_ref.hpp>
//#include "misc.h"

#include <map>
#include <iostream>
#include <memory>
#include <algorithm>

using String      = std::string;
using StringView  = boost::string_ref;

struct Interface
{
    virtual StringView name() const = 0;

    virtual ~Interface() {}
};

Interface * make_interface(String name)
{
    struct Impl : Interface
    {
        StringView name() const override
        {
            return name_;
        }

        Impl(String && name)
            : name_(std::move(name))
        {}

    private:
        String name_;
    };

    return new Impl(std::move(name));
}

#define SOLUTION_1

#ifdef SOLUTION_1
/// XXX solution: use shared pointers
struct Key
{
    explicit Key(Interface * i)
        : i_(i)
    {}

    ~Key() {}

    Interface const & get_interface() const { return *i_; }

private:
    std::shared_ptr<Interface> i_;
};

#else
/// XXX solution: prohibit copy contstructor
/// XXX dirty hack!!! This is ill-specified key class
/// cppref requires that key is COPYASSIGNABLE
struct Key
{
    explicit Key(Interface * i)
        : i_(i)
    {}

    Key(const Key &) = delete;

    ~Key()
    {
        delete i_;
    }

    Interface const & get_interface() const { return *i_; }

private:
    Interface* i_;
};
#endif

bool operator < (Key const & a, Key const & b)
{
    return a.get_interface().name() < b.get_interface().name();
}

int main()
{
    std::map<Key, int> map;
    map.emplace(std::piecewise_construct, std::forward_as_tuple(make_interface("one")),   std::forward_as_tuple(1));
    map.emplace(std::piecewise_construct, std::forward_as_tuple(make_interface("two")),   std::forward_as_tuple(2));
    map.emplace(std::piecewise_construct, std::forward_as_tuple(make_interface("three")), std::forward_as_tuple(3));

    int sum = 0;

#ifdef SOLUTION_1
    for (std::pair<Key, int> const & kv : map)
#else
    for (auto const & kv : map)
#endif
        sum += kv.second;

    std::cout << sum << std::endl;
}
