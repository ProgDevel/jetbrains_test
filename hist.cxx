#include <map>
#include <future>
#include <iostream>
#include <iomanip>
#include <random>

/// following code has been left 'AS IS'

using Histogram = std::map<int, int>;

void draw(Histogram const & hist)
{
    for (auto const & p : hist)
    {
        std::cout << std::fixed << std::setprecision(1) << std::setw(2)
            << p.first << ' ' << std::string(p.second / 200, '*') << '\n';

    }

}

int main()
{
    std::random_device rdevice;
    std::default_random_engine rengine(rdevice());
    std::normal_distribution<double> dist(0., 1.);
    std::vector<std::future<double>> samples;
    for (size_t i = 10000; i --> 0; )
        samples.push_back(std::async(std::launch::deferred, std::bind(dist, rengine)));

    Histogram hist;
    for (auto & sample : samples)
        ++hist[std::round(sample.get())];
    draw(hist);

}
